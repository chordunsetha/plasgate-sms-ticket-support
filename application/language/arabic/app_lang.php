<?php

/*
 * Arabic language
 */

$lang['text_profile'] = "الملف الشخصي";
$lang['text_logout'] = "تسجيل خروج";
$lang['text_english'] = "الإنجليزية";
$lang['text_french'] = "فرنسي";
$lang['text_arabic'] = "عربى";
$lang['text_dashboard'] = "لوحة القيادة";
$lang['text_knowledge_base'] = "قاعدة المعرفة";
$lang['text_articles'] = "مقالات";
$lang['text_article_categories'] = "فئات المقالات";
$lang['text_tickets'] = "التذاكر";
$lang['text_list_tickets'] = "قائمة التذاكر";
$lang['text_ticket_categories'] = "فئات التذاكر";
$lang['text_faq'] = "التعليمات";
$lang['text_list_faq'] = "قائمة الأسئلة الشائعة";
$lang['text_faq_categories'] = "فئات الأسئلة الشائعة";
$lang['text_users'] = "المستخدمون";
$lang['text_list_users'] = "سرد المستخدمين";
$lang['text_user_permissions'] = "أذونات المستخدم";
$lang['text_settings'] = "الإعدادات";
$lang['text_site_settings'] = "إعدادات الموقع";
$lang['text_social_media_settings'] = "إعدادات الوسائط الاجتماعية";
$lang['text_seo_settings'] = "إعدادات تحسين محركات البحث";
$lang['text_permissions'] = "أذونات";
$lang['text_app_settings'] = "إعدادات التطبيقات";
$lang['text_dashboard_subtitle'] = "نظرة عامة على التطبيق";
$lang['text_latest_articles'] = "أحدث المقالات";
$lang['text_unpublished'] = "غير منشورة";
$lang['text_published'] = "نشرت";
$lang['text_likes'] = "الإعجابات";
$lang['text_dislikes'] = "يكره";
$lang['text_not_found_articles'] = "لم يتم العثور على مقالات!";
$lang['text_latest_tickets'] = "أحدث التذاكر";
$lang['text_new'] = "جديد";
$lang['text_in_progress'] = "في تقدم";
$lang['text_closed'] = "مغلق";
$lang['text_low'] = "منخفض";
$lang['text_medium'] = "متوسط";
$lang['text_high'] = "عالي";
$lang['text_urgent'] = "العاجلة";
$lang['text_assigned_to'] = "مخصص ل";
$lang['text_undefined'] = "غير معرف";
$lang['text_no_tickets_found'] = "لم يتم العثور على تذاكر!";
$lang['text_profile_settings'] = "إعدادات الملف الشخصي";
$lang['text_full_name'] = "الاسم الكامل";
$lang['text_enter_full_name'] = "أدخل الاسم الكامل";
$lang['text_email'] = "البريد الإلكتروني";
$lang['text_enter_email'] = "أدخل البريد الإلكتروني";
$lang['text_mobile'] = "التليفون المحمول";
$lang['text_enter_mobile'] = "أدخل الجوال";
$lang['text_update_profile'] = "تحديث الملف";
$lang['text_success'] = "نجاح";
$lang['text_error'] = "خطأ";
$lang['text_change_password'] = "غير كلمة السر";
$lang['text_change_password_subtitle'] = "تحديث كلمة مرور تسجيل الدخول";
$lang['text_old_password'] = "كلمة المرور القديمة";
$lang['text_enter_old_password'] = "أدخل كلمة المرور القديمة";
$lang['text_new_password'] = "كلمة سر جديدة";
$lang['text_enter_new_password'] = "أدخل كلمة مرور جديدة";
$lang['text_confirm_new_password'] = "تأكيد كلمة المرور الجديدة";
$lang['text_crop_image'] = "تأكيد كلمة المرور الجديدة";
$lang['text_crop'] = "ا & قتصاص";
$lang['text_cancel'] = "إلغاء";
$lang['text_articles_subtitle'] = "قائمة المواد";
$lang['text_filters'] = "مرشحات";
$lang['text_keyword'] = "الكلمة الرئيسية";
$lang['text_enter_keyword'] = "أدخل الكلمة المفتاحية";
$lang['text_category'] = "الفئة";
$lang['text_select_category'] = "اختر الفئة";
$lang['text_status'] = "الحالة";
$lang['text_select_status'] = "حدد الحالة";
$lang['text_filter_articles'] = "تصفية المقالات";
$lang['text_add_article'] = "أضف مقال";
$lang['text_articles_list'] = "قائمة المقالات";
$lang['text_articles_ordering'] = "ترتيب المقالات";
$lang['text_article_category'] = "فئة المقالة";
$lang['text_loading'] = "جار التحميل";
$lang['text_view_article'] = "عرض المادة";
$lang['text_edit_article'] = "تحرير المقال";
$lang['text_title'] = "عنوان";
$lang['text_enter_title'] = "أدخل عنوان المقال";
$lang['text_excerpt'] = "مقتطفات";
$lang['text_enter_excerpt'] = "أدخل مقتطف المقالة";
$lang['text_content'] = "المحتوى";
$lang['text_enter_content'] = "أدخل المحتوى";
$lang['text_publish'] = "ينشر";
$lang['text_unpublish'] = "غير منشور";
$lang['text_view'] = "رأي";
$lang['text_edit'] = "تعديل";
$lang['text_delete'] = "حذف";
$lang['text_create_article'] = "إنشاء مقال";
$lang['text_update_article'] = "تحديث المادة";
$lang['text_publish_article'] = "نشر مقال";
$lang['text_unpublish_article'] = "مقالة غير منشورة";
$lang['text_publish_article_info'] = "بالضغط على 'نشر' سيتم نشر مقال على البوابة.";
$lang['text_unpublish_article_info'] = "بالنقر على 'إلغاء النشر' ، سيتم إلغاء نشر المقالة من البوابة.";
$lang['text_article_ordering'] = "ترتيب المادة";
$lang['text_delete_article'] = "تحذف المادة";
$lang['text_delete_article_info'] = "عن طريق النقر فوق 'حذف' المقالة كاملةحذف موقع النموذج ، يمكنك استرداد مرة أخرى";
$lang['text_save_order'] = "حفظ الطلب";
$lang['text_created_on'] = "تاريخ الإنشاء";
$lang['text_updated_on'] = "آخر تحديث";
$lang['text_view_article_category'] = "عرض فئة المقالة";
$lang['text_add_article_category'] = "أضف فئة المقالة";
$lang['text_edit_article_category'] = "تحرير فئة المقالة";
$lang['text_categories_ordering'] = "ترتيب الفئات";
$lang['text_delete_article_category'] = "حذف فئة المقالة";
$lang['text_article_categories_subtitle'] = "قائمة فئات المقالات";
$lang['text_categories_list'] = "قائمة الفئات";
$lang['text_add_category'] = "إضافة فئة";
$lang['text_categories_not_found'] = "لم يتم العثور على فئات!";
$lang['text_enter_category_title'] = "أدخل عنوان الفئة";
$lang['text_description'] = "وصف";
$lang['text_enter_description'] = "أدخل الوصف";
$lang['text_category_icon'] = "أيقونة الفئة";
$lang['text_category_icon_info'] = "قم بتحميل الصور فقط * .png و * .jpg و * .gif وحجم الملف الأقصى 1 ميجا بايت";
$lang['text_choose_file'] = "اختر ملف";
$lang['text_save_category'] = "حفظ الفئة";
$lang['text_update_category'] = "تحديث الفئة";
$lang['text_delete_article_category_info'] = "عن طريق النقر فوق 'حذف' فئة المقالة كاملةحذف نموذج الموقع ، يمكنك استرداد مرة أخرى";
$lang['text_complete_delete'] = "أكمل الحذف";
$lang['text_transfer_and_delete'] = "نقل وحذف";
$lang['text_article_category_transfer_and_delete_info'] = "تحتوي هذه الفئة على بعض المقالات ، يرجى تحديد فئة أخرى والنقر فوق 'نقل وحذف' لنقل المقالات إلى تلك الفئة أو انقر فوق 'إكمال الحذف' لحذف الفئة والمقالات داخل الفئة";
$lang['text_view_ticket'] = "عرض التذكرة";
$lang['text_assign_ticket'] = "قم بتعيين التذكرة";
$lang['text_mark_ticket_completed'] = "ضع علامة على التذكرة على أنها مكتملة";
$lang['text_mark_ticket_incompleted'] = "ضع علامة على التذكرة على أنها غير مكتملة";
$lang['text_delete_ticket'] = "حذف التذكرة";
$lang['text_view_ticket_category'] = "عرض فئة التذكرة";
$lang['text_add_ticket_category'] = "إضافة فئة تذكرة";
$lang['text_edit_ticket_category'] = "تحرير فئة التذكرة";
$lang['text_delete_ticket_category'] = "حذف فئة التذكرة";
$lang['text_tickets_subtitle'] = "قائمة التذاكر";
$lang['text_ticket_priority'] = "أولوية التذكرة";
$lang['text_select_ticket_priority'] = "حدد أولوية التذكرة";
$lang['text_ticket_type'] = "نوع التذكرة";
$lang['text_select_ticket_type'] = "حدد نوع التذكرة";
$lang['text_customer_tickets'] = "تذاكر العملاء";
$lang['text_guest_tickets'] = "تذاكر الضيوف";
$lang['text_filter_tickets'] = "تصفية التذاكر";
$lang['text_view_and_reply'] = "العرض والرد";
$lang['text_assign'] = "تعيين";
$lang['text_mark_completed'] = 'وضع علامة "مكتمل"';
$lang['text_mark_incompleted'] = 'يتم وضع علامة "غير مكتمل"';
$lang['text_select_user'] = "اختر المستخدم";
$lang['text_complete'] = "اكتمال";
$lang['text_complete_info'] = "من خلال النقر على 'مكتمل' سيتم وضع علامة 'مكتمل' على البطاقة على أنها مكتملة!";
$lang['text_incomplete'] = "غير مكتمل";
$lang['text_incomplete_info'] = "من خلال النقر على 'غير مكتمل' سيتم وضع علامة 'مكتمل' على التذكرة!";
$lang['text_delete_ticket_info'] = "عن طريق النقر فوق 'حذف' تذكرة كاملة حذف موقع النموذج المحذوف ، لا يمكنك استرداد مرة أخرى";
$lang['text_ticket_info'] = "معلومات التذكرة";
$lang['text_reply_to_ticket'] = "الرد على التذكرة";
$lang['text_guest'] = "الرد على التذكرة";
$lang['text_download_attachment'] = "تنزيل المرفقات";
$lang['text_reply'] = "الرد";
$lang['text_attachment'] = "المرفق";
$lang['text_ticket_attachment_info'] = "تحميل ملفات * .png و * .jpg و * .gif و * .doc و * .pdf فقط.";
$lang['text_replies'] = "الردود";
$lang['text_no_replies_found'] = "لا توجد ردود!";
$lang['text_delete_ticket_category_info']="من خلال النقر فوق 'حذف' فئة التذكرة بالكامل ، حذف موقع النموذج المحذوف ، لا يمكنك استرداد مرة أخرى";
$lang['text_ticket_category_transfer_and_delete_info'] ="تحتوي هذه الفئة على بعض التذاكر ، يرجى تحديد فئة أخرى والنقر فوق 'نقل وحذف' لنقل التذاكر إلى تلك الفئة أو انقر فوق 'إكمال الحذف' لحذف الفئة والتذاكر داخل الفئة";
$lang['text_view_faq'] ="عرض التعليمات";
$lang['text_add_faq'] ="أضف التعليمات";
$lang['text_edit_faq'] ="تحرير الأسئلة المتداولة";
$lang['text_publish_faq'] ="نشر الأسئلة الشائعة";
$lang['text_unpublish_faq'] ="الأسئلة الشائعة غير المنشورة";
$lang['text_faq_ordering'] ="ترتيب التعليمات";
$lang['text_delete_faq'] ="حذف الأسئلة الشائعة";
$lang['text_view_faq_category'] = "عرض فئة الأسئلة الشائعة";
$lang['text_add_faq_category'] = "إضافة فئة الأسئلة الشائعة";
$lang['text_edit_faq_category'] = "تعديل فئة الأسئلة الشائعة";
$lang['text_delete_faq_category'] = "حذف فئة الأسئلة الشائعة";
$lang['text_faq_subtitle'] = "قائمة الأسئلة الشائعة";
$lang['text_filter_faq'] = "الأسئلة الشائعة حول التصفية";
$lang['text_faq_list'] = "قائمة الأسئلة الشائعة";
$lang['text_faq_category'] = "فئة الأسئلة الشائعة";
$lang['text_create_faq'] = "إنشاء الأسئلة الشائعة";
$lang['text_no_faq_found'] = "لا توجد أسئلة شائعة!";
$lang['text_delete_faq_info']="بالضغط على 'حذف' الأسئلة المتداولة بالكامل حذف نموذج الموقع ، لا يمكنك استرداد مرة أخرى";
$lang['text_delete_faq_category_info']="بالضغط على 'حذف' الأسئلة المتكررة فئة حذف موقع النموذج المحذوف ، لا يمكنك استعادة";
$lang['text_faq_category_transfer_and_delete_info']="تحتوي هذه الفئة على بعض الأسئلة الشائعة ، يرجى تحديد فئة أخرى والنقر فوق 'نقل وحذف' لنقل الأسئلة الشائعة إلى تلك الفئة أو انقر فوق 'إكمال الحذف' لحذف الفئة والأسئلة الشائعة داخل الفئة";
$lang['text_update_faq'] = "تحديث الأسئلة المتداولة";
$lang['text_faq_categories_subtitle'] = "قائمة فئات الأسئلة الشائعة";
$lang['text_publish_faq_info'] = "بالضغط على 'نشر' سيتم نشر التعليمات على البوابة.";
$lang['text_unpublish_faq_info'] = "بالنقر فوق 'إلغاء النشر' ، سيتم إلغاء نشر الأسئلة المتداولة من المدخل.";
$lang['text_view_user'] = "عرض المستخدم";
$lang['text_add_user'] = "أضف مستخدم";
$lang['text_edit_user'] = "تحرير العضو";
$lang['text_block_user'] = "مستخدم محضور";
$lang['text_unblock_user'] = "افتح قفل المستخدم";
$lang['text_delete_user'] = "مسح المستخدم";
$lang['text_users_subtitle'] = "قائمة المستخدمين";
$lang['text_enter_part_username'] = "أدخل جزءًا من اسم المستخدم";
$lang['text_users_type'] = "نوع المستخدم";
$lang['text_select_users_type'] = "حدد نوع المستخدم";
$lang['text_inactive'] = "غير نشط";
$lang['text_active'] = "نشيط";
$lang['text_blocked'] = "محظور";
$lang['text_filter_users'] = "تصفية المستخدمين";
$lang['text_add_user'] = "أضف مستخدم";
$lang['text_password'] = "كلمه السر";
$lang['text_enter_password'] = "أدخل كلمة المرور";
$lang['text_create_user'] = "إنشاء مستخدم";
$lang['text_block'] = "منع";
$lang['text_block_user_info'] = "بالنقر على 'حظر' ، سيتم حظر المستخدم من التطبيق";
$lang['text_unblock'] = "رفع الحظر";
$lang['text_unblock_user_info'] = "بالنقر فوق 'إلغاء الحظر' ، سيتم حظر المستخدم من التطبيق";
$lang['text_delete_user_info'] = "بالنقر فوق 'حذف' المستخدم بالكامل حذف موقع النموذج المحذوف ، لا يمكنك استرداد مرة أخرى";
$lang['text_update_user'] = "تحديث المستخدم";
$lang['text_permission_name'] = "اسم الإذن";
$lang['text_action'] = "عمل";
$lang['text_no_permission_found'] = "لم يتم العثور على إذن!";
$lang['text_no_users_found'] = "لم يتم العثور على مستخدمين!";
$lang['text_users_and_permissions'] = "المستخدمون والأذونات";
$lang['text_search'] = "بحث";
$lang['text_filter_permissions'] = "أذونات التصفية";
$lang['text_user_permissions_info'] = "حدد المستخدم وانقر فوق 'تصفية الأذونات' لتحميل الأذونات";
$lang['text_last_login'] = "آخر تسجيل دخول";
$lang['text_last_login_ip_address'] = "آخر عنوان IP لتسجيل الدخول";
$lang['text_last_login_agent'] = "وكيل تسجيل الدخول الأخير";
$lang['text_site_logo'] = "شعار الموقع";
$lang['text_site_logo_info'] = "قم بتحميل الصور فقط * .png و * .jpg و * .gif وحجم الملف الأقصى 1 ميجا بايت";
$lang['text_site_favicon'] = "موقع فافيكون";
$lang['text_site_favicon_info'] = "تحميل الصور فقط * .png ، * .jpg ، * .gif ، * .ico ، الحد الأقصى لحجم الملف 1 ميغابايت";
$lang['text_site_title'] = "عنوان الموقع";
$lang['text_enter_site_title'] = "أدخل عنوان الموقع";
$lang['text_site_email'] = "البريد الإلكتروني للموقع";
$lang['text_enter_site_email'] = "أدخل البريد الإلكتروني للموقع";
$lang['text_site_phone'] = "هاتف الموقع";
$lang['text_enter_site_phone'] = "أدخل هاتف الموقع";
$lang['text_save_settings'] = "احفظ التغييرات";
$lang['text_site_settings_subtitle'] = "تحديث معلومات الموقع";
$lang['text_social_media_settings_subtitle'] = "تحديث معلومات الموقع";
$lang['text_facebook'] = "موقع التواصل الاجتماعي الفيسبوك";
$lang['text_facebook_link'] = "أدخل رابط Facebook";
$lang['text_twitter'] = "تويتر";
$lang['text_twitter_link'] = "أدخل رابط Twitter";
$lang['text_instagram'] = "انستغرام";
$lang['text_instagram_link'] = "أدخل رابط Instagram";
$lang['text_linkedin'] = "لينكد إن";
$lang['text_linkedin_link'] = "أدخل Linkedin Link";
$lang['text_google_plus'] = "جوجل بلس";
$lang['text_linkedin_link'] = "أدخل رابط Google Plus";
$lang['text_youtube'] = "موقع YouTube";
$lang['text_youtube_link'] = "أدخل رابط يوتيوب";
$lang['text_github'] = "جيثب";
$lang['text_github_link'] = "أدخل رابط جيثب";
$lang['text_seo_settings_subtitle'] = "إعدادات تحسين محرك البحث";
$lang['text_meta_title'] = "عنوان الفوقية";
$lang['text_enter_meta_title'] = "أدخل عنوان التعريف";
$lang['text_meta_description'] = "ميتا الوصف";
$lang['text_enter_meta_description'] = "أدخل وصف Meta";
$lang['text_meta_keywords'] = "كلمات دلالية";
$lang['text_enter_meta_keywords'] = "أدخل الكلمات الأساسية لـ Meta";
$lang['text_google_analytics'] = "كود جوجل تحليلات";
$lang['text_enter_google_analytics'] = "أدخل رمز Google Analytics";
$lang['text_permissions_subtitle'] = "أدخل رمز Google Analytics";
$lang['text_permission'] = "الأدوار والأذونات";
$lang['text_support_agent'] = "وكيل الدعم";
$lang['text_support_manager'] = "مدير الدعم";
$lang['text_app_settings_subtitle'] = "تكوينات التطبيق الأساسية";
$lang['text_email_notify_ticket_create'] = "إخطار البريد الإلكتروني عند إنشاء تذكرة جديدة";
$lang['text_email_notify_ticket_assign'] = "إخطار البريد الإلكتروني عند تعيين التذكرة";
$lang['text_password_notify_user_create'] = "إرسال كلمة المرور إلى البريد الإلكتروني عند إنشاء مستخدم جديد";
$lang['text_next']="التالى";
$lang['text_previous']="السابق";
$lang['text_last']="الاخير";
$lang['text_first']="أول";
$lang['text_signin_title']="قم بتسجيل الدخول إلى حسابك";
$lang['text_remember_password']="تذكر كلمة المرور";
$lang['text_signin']="تسجيل الدخول";
$lang['text_helpme']="ساعدني !.";
$lang['text_i_forgot_password']="نسيت كلمة المرور!";
$lang['text_reset_password_title']="اعد ضبط كلمه السر";
$lang['text_reset_password']="إعادة تعيين كلمة المرور";
$lang['text_forgot_password_link']="احصل على رابط إعادة تعيين كلمة المرور";
$lang['text_send_reset_link']="إرسال رابط إعادة تعيين";
$lang['text_login']="تسجيل الدخول";
$lang['text_backto']="ارجع الى";
$lang['text_submit_ticket']="قدم التذكرة";
$lang['text_back_to_list']="الرجوع للقائمة";
$lang['text_ticket_title']="عنوان التذكرة";
$lang['text_enter_ticket_title']="أدخل عنوان التذكرة";
$lang['text_ticket_description']="وصف التذكرة";
$lang['text_enter_ticket_description']="أدخل وصف التذكرة";
$lang['text_create_ticket']="إنشاء تذكرة";
$lang['text_home']="الصفحة الرئيسية";
$lang['text_contact']="اتصل";
$lang['text_contact_subtitle']="تواصل معنا";
$lang['text_subject']="موضوع";
$lang['text_enter_subject']="أدخل الموضوع";
$lang['text_message']="رسالة";
$lang['text_enter_message']="أدخل رسالة";
$lang['text_send_message']="أرسل رسالة";
$lang['text_hello']="مرحبا";
$lang['text_all_tickets']="جميع التذاكر";
$lang['text_new_tickets']="تذاكر جديدة";
$lang['text_progress_tickets']="تذاكر قيد التقدم";
$lang['text_closed_tickets']="تذاكر مغلقة";
$lang['text_allow_guest_ticket_submission']="السماح بتقديم تذاكر الضيوف";
$lang['text_articles']="مقالات";
$lang['text_search_here']="ابحث هنا";
$lang['text_created']="خلقت";
$lang['text_updated']="محدث";
$lang['text_was_article_helpfull']="هل كان المقال مساعدا؟!";
$lang['text_yes']="نعم";
$lang['text_no']="لا";
$lang['text_marked_helpfull']="تم وضع علامة مفيدًا";
$lang['text_out_of']="بعيدا عن المكان";
$lang['text_have_more_questions']="هل لديك المزيد من الأسئلة؟";
$lang['text_contact_us']="الرجاء التواصل معنا";
$lang['text_recent_articles']="المقالات الأخيرة";
$lang['text_no_articles_found']="لم يتم العثور على مقالات!";
$lang['text_categories']="التصنيفات";
$lang['text_no_categories_found']="لم يتم العثور على فئات!";
$lang['text_looking_for_join']="البحث عن مساعدة؟ انضم إلى المجتمع";
$lang['text_looking_for_join_info']="لم تجد ما تبحث عنه؟ لماذا لا تنضم إلى نظام الدعم ودعنا نساعدك ";
$lang['text_register_now']="سجل الان! ";
$lang['text_faq']="التعليمات";
$lang['text_faq_full']="أسئلة مكررة";
$lang['text_main_title']="نظام تذاكر دعم TrueSupport";
$lang['text_main_sub_title']="حصريا لدعم تذاكر وقواعد المعرفة";
$lang['text_suggested_articles']="مقالات مقترحة";
$lang['text_explore_topics']="استكشاف المواضيع";
$lang['text_explore_topics_info']="لقد بذلنا قصارى جهدنا لتغطية جميع الموضوعات المتعلقة بهذا المنتج. يحتوي كل قسم على عدد يمثل عدد الموضوعات في كل فئة.";
$lang['text_explore_all']="استكشاف الكل";
$lang['text_knowledge_base']="قاعدة المعرفة";
$lang['text_view_all']="عرض الكل";
$lang['text_submit_ticket']="قدم التذكرة";
$lang['text_submit_ticket_subtitle']="قم بإنشاء تذكرة دعم جديدة";
$lang['text_search']="بحث";
$lang['text_search_result']="نتيجة البحث";
$lang['text_search_result_info']="قائمة المادة في إعطاء الكلمة";
$lang['text_activation']="التنشيط";
$lang['text_activation_code']="رمز التفعيل";
$lang['text_enter_activation_code']="أدخل رمز التفعيل";
$lang['text_activation_code_info']="رمز التفعيل أرسل إلى بريدك الإلكتروني. تحقق من صندوق البريد";
$lang['text_activate']="تفعيل";
$lang['text_forgot_password']="هل نسيت كلمة المرور";
$lang['text_register']="تسجيل";
$lang['text_reset_password']="إعادة تعيين كلمة المرور";
$lang['text_inprogress_tickets']="تذاكر قيد التقدم";
$lang['text_email_settings']="إعدادات البريد الإلكتروني";
$lang['text_email_settings_subtitle']="تكوين إعدادات البريد الإلكتروني الخاص بك";
$lang['text_mail_from_title']="البريد من العنوان";
$lang['text_enter_mail_from_title']="أدخل البريد من العنوان";
$lang['text_mail_driver']="برنامج تشغيل البريد";
$lang['text_mail_port']="منفذ البريد";
$lang['text_enter_mail_port']="أدخل منفذ البريد";
$lang['text_mail_password']="كلمة مرور البريد";
$lang['text_enter_mail_password']="أدخل كلمة مرور البريد";
$lang['text_mail_from_mail']="البريد من البريد الإلكتروني";
$lang['text_enter_mail_from_mail']="أدخل البريد من البريد الإلكتروني";
$lang['text_mail_host']="مضيف البريد";
$lang['text_enter_mail_host']="أدخل مضيف البريد";
$lang['text_mail_username']="اسم مستخدم البريد";
$lang['text_enter_mail_username']="أدخل اسم مستخدم البريد";
$lang['text_mail_encryption']="تشفير البريد";
$lang['text_select_one']="حدد واحد";
$lang['text_email_templates']="قوالب البريد الإلكتروني";
$lang['text_email_templates_subtitle']="إدارة قوالب البريد الإلكتروني الخاص بك";
$lang['text_edit_email_template']="تحرير قالب البريد الإلكتروني";
$lang['text_templates_not_found']="لم يتم العثور على القوالب";
$lang['text_template_name']="اسم القوالب";
$lang['text_enter_template_name']="أدخل اسم القوالب";
$lang['text_template_content']="محتوى القوالب";
$lang['text_template_variables']="المتغيرات القابلة للاستخدام:";
$lang['text_template_name_info']="يعتبر اسم القالب موضوع البريد";
$lang['text_update_template']="تحديث القالب";


//Alerts
$lang['alert_went_wrong'] = "هناك خطأ ما!";
$lang['alert_enter_fullname'] = "من فضلك ادخل الاسم الكامل!";
$lang['alert_enter_email'] = "يرجى إدخال البريد الإلكتروني!";
$lang['alert_enter_valid_email'] = "أدخل عنوان بريد إلكتروني صالح!";
$lang['alert_enter_valid_email_lenght'] = "يجب أن يكون طول البريد الإلكتروني 4 أحرف على الأقل!";
$lang['alert_enter_mobile'] = "يرجى إدخال رقم الهاتف المحمول!";
$lang['alert_profile_updated'] = "تم تحديث الملف الشخصي بنجاح!";
$lang['alert_profile_not_updated'] = "خطأ في تحديث الملف الشخصي!";
$lang['alert_profile_email_exist'] = "يوجد مستخدم آخر في بريد إلكتروني معين!";
$lang['alert_profile_image_updated'] = "تم تحديث صورة الملف الشخصي بنجاح!";
$lang['alert_profile_image_not_updated'] = "خطأ في تحديث صورة الملف الشخصي!";
$lang['alert_enter_old_password'] = "الرجاء إدخال كلمة المرور القديمة!";
$lang['alert_enter_new_password'] = "يرجى إدخال كلمة مرور جديدة";
$lang['alert_enter_new_password_minlength'] = "يجب أن تحتوي كلمة المرور على 8 أحرف على الأقل";
$lang['alert_enter_new_password_maxlength'] = "لا تتجاوز كلمة المرور 15 حرفًا";
$lang['alert_enter_confirm_password'] = "يرجى إدخال كلمة مرور جديدة";
$lang['alert_enter_confirm_password_minlength'] = "يجب أن تحتوي كلمة المرور على 8 أحرف على الأقل";
$lang['alert_enter_confirm_password_maxlength'] = "لا تتجاوز كلمة المرور 15 حرفًا";
$lang['alert_enter_password_repeat'] = "يرجى إعادة كلمة المرور نفسها";
$lang['alert_password_updated'] = "تم تحديث كلمة المرور بنجاح!";
$lang['alert_password_not_updated'] = "خطأ في تحديث كلمة المرور!";
$lang['alert_old_password_incorrect'] = "كلمة سر قديمة ليست صحيحة!";
$lang['alert_select_category'] = "الرجاء تحديد الفئة";
$lang['alert_loading_list_error'] = "خطأ في قائمة التحميل!";
$lang['alert_load_view_error'] = "خطأ في تحميل العرض!";
$lang['alert_enter_title'] = "من فضلك ادخل العنوان!";
$lang['alert_enter_description'] = "الرجاء إدخال الوصف!";
$lang['alert_enter_excerpt'] = "يرجى إدخال مقتطفات!";
$lang['alert_article_created'] = "المادة التي تم إنشاؤها بنجاح!";
$lang['alert_article_not_created'] = "خطأ في إنشاء مقال!";
$lang['alert_article_updated'] = "تحديث المادة بنجاح!";
$lang['alert_article_not_updated'] = "خطأ في تحديث المقال!";
$lang['alert_access_denied'] = "تم الرفض!";
$lang['alert_article_published'] = "مقال نشر بنجاح!";
$lang['alert_article_not_published'] = "خطأ في نشر المقال!";
$lang['alert_article_unpublished'] = "مقالة غير منشورة بنجاح!";
$lang['alert_article_order_updated'] = "تم تحديث ترتيب المقالة بنجاح!";
$lang['alert_article_order_not_updated'] = "خطأ في تحديث ترتيب المقالات!";
$lang['alert_article_deleted'] = "تم حذف المقالة بنجاح!";
$lang['alert_article_not_deleted'] = "خطأ في حذف المقال!";
$lang['alert_article_category_created'] = "تم إنشاء فئة المقالة بنجاح!";
$lang['alert_article_category_not_created'] = "خطأ في إنشاء فئة المقالة!";
$lang['alert_article_category_updated'] = "تم تحديث فئة المقالة بنجاح!";
$lang['alert_article_category_not_updated'] = "خطأ في تحديث فئة المقالة!";
$lang['alert_article_category_ordering_updated'] = "تم تحديث ترتيب الفئات بنجاح!";
$lang['alert_article_category_ordering_not_updated'] = "خطأ في تحديث ترتيب الفئات!";
$lang['alert_article_category_deleted'] = "تم حذف فئة المقالة بنجاح!";
$lang['alert_article_category_not_deleted'] = "خطأ في حذف فئة المقالة!";
$lang['alert_transfer_category_not_exist'] = "نقل الفئة غير موجود!";
$lang['alert_replied_to_ticket'] = "تم الرد على التذكرة بنجاح!";
$lang['alert_not_replied_to_ticket'] = "خطأ في الرد على التذكرة!";
$lang['alert_ticket_assigned'] = "تم تعيين التذكرة بنجاح للمستخدم!";
$lang['alert_ticket_not_assigned'] = "خطأ في تعيين تذكرة للمستخدم!";
$lang['alert_marked_completed'] = "تم بنجاح وضع علامة مكتمل!";
$lang['alert_not_marked_completed'] = "خطأ في وضع علامة مكتمل!";
$lang['alert_marked_incompleted'] = "تم وضع علامة بنجاح على أنه غير مكتمل!";
$lang['alert_not_marked_incompleted'] = "خطأ في وضع علامة غير مكتملة!";
$lang['alert_ticket_deleted'] = "تم حذف التذكرة بنجاح!";
$lang['alert_ticket_not_deleted'] = "خطأ في حذف التذكرة!";
$lang['alert_ticket_category_created'] = "فئة تذكرة تم إنشاؤها بنجاح!";
$lang['alert_ticket_category_not_created'] = "خطأ في إنشاء فئة التذكرة!";
$lang['alert_ticket_category_updated'] = "تم تحديث فئة التذكرة بنجاح!";
$lang['alert_ticket_category_not_updated'] = "خطأ في تحديث فئة التذكرة!";
$lang['alert_ticket_category_order_updated'] = "تم تحديث ترتيب الفئات بنجاح!";
$lang['alert_ticket_category_order_not_updated'] = "خطأ في تحديث ترتيب الفئات!";
$lang['alert_ticket_category_deleted'] = "تم حذف فئة التذكرة بنجاح!";
$lang['alert_ticket_category_not_deleted'] = "خطأ في حذف فئة التذكرة!";
$lang['alert_enter_reply'] = "يرجى إدخال رد";
$lang['alert_select_user'] = "يرجى تحديد المستخدم";
$lang['alert_select_priority'] = "يرجى تحديد الأولوية";
$lang['alert_faq_created'] = "الأسئلة المتداولة التي تم إنشاؤها بنجاح!";
$lang['alert_faq_not_created'] = "خطأ في إنشاء الأسئلة الشائعة!";
$lang['alert_faq_updated'] = "الأسئلة الشائعة المحدثة بنجاح!";
$lang['alert_faq_not_updated'] = "خطأ في تحديث الأسئلة المتداولة!";
$lang['alert_faq_published'] = "الأسئلة الشائعة التي تم نشرها بنجاح!";
$lang['alert_faq_not_published'] = "خطأ في نشر الأسئلة الشائعة!";
$lang['alert_faq_unpublished'] = "الأسئلة الشائعة غير المنشورة بنجاح!";
$lang['alert_faq_not_unpublished'] = "خطأ في إلغاء نشر الأسئلة الشائعة!";
$lang['alert_faq_order_updated'] = "تم تحديث الأسئلة المتداولة بنجاح!";
$lang['alert_faq_order_not_updated'] = "خطأ في تحديث طلب الأسئلة الشائعة!";
$lang['alert_faq_deleted'] = "الأسئلة الشائعة المحذوفة بنجاح!";
$lang['alert_faq_not_deleted'] = "خطأ في حذف الأسئلة الشائعة!";
$lang['alert_faq_category_created'] = "فئة الأسئلة المتداولة التي تم إنشاؤها بنجاح!";
$lang['alert_faq_category_not_created'] = "خطأ في إنشاء فئة الأسئلة الشائعة!";
$lang['alert_faq_category_updated'] = "تم تحديث فئة الأسئلة الشائعة بنجاح!";
$lang['alert_faq_category_not_updated'] = "خطأ في تحديث فئة الأسئلة الشائعة!";
$lang['alert_faq_category_order_updated'] = "تم تحديث ترتيب الفئات بنجاح!";
$lang['alert_faq_category_order_not_updated'] = "خطأ في تحديث ترتيب الفئات!";
$lang['alert_faq_category_deleted'] = "تم حذف فئة الأسئلة الشائعة بنجاح!";
$lang['alert_faq_category_not_deleted'] = "خطأ في حذف فئة الأسئلة الشائعة!";
$lang['alert_enter_content'] = "يرجى إدخال المحتوى";
$lang['alert_user_created'] = "تم إنشاء المستخدم بنجاح!";
$lang['alert_user_not_created'] = "خطأ في إنشاء المستخدم!";
$lang['alert_user_exist'] = "المستخدم مع البريد الإلكتروني موجود بالفعل!";
$lang['alert_user_updated'] = "تم تحديث المستخدم بنجاح!";
$lang['alert_user_not_updated'] = "خطأ في تحديث المستخدم!";
$lang['alert_user_blocked'] = "مستخدم محظور بنجاح!";
$lang['alert_user_not_blocked'] = "خطأ في حظر المستخدم!";
$lang['alert_user_unblocked'] = "مستخدم غير محظور بنجاح!";
$lang['alert_user_not_unblocked'] = "خطأ في إلغاء حظر المستخدم!";
$lang['alert_user_deleted'] = "تم حذف المستخدم بنجاح!";
$lang['alert_user_not_deleted'] = "خطأ في حذف المستخدم!";
$lang['alert_user_permission_updated'] = "تم تحديث إذن المستخدم بنجاح!";
$lang['alert_user_permission_not_updated'] = "خطأ في تحديث إذن المستخدم!";
$lang['alert_select_user_type'] = "يرجى تحديد نوع المستخدم";
$lang['alert_enter_password'] = "يرجى إدخال كلمة المرور";
$lang['alert_valid_password_lenght'] = "يجب أن تتكون كلمة المرور من 8 أحرف على الأقل";
$lang['alert_select_user'] = "يرجى تحديد مستخدم!";
$lang['alert_site_settings_updated'] = "تم تحديث إعدادات الموقع بنجاح!";
$lang['alert_site_settings_not_updated'] = "خطأ في تحديث إعدادات الموقع!";
$lang['alert_social_media_settings_updated'] = "تم تحديث إعدادات الوسائط الاجتماعية بنجاح!";
$lang['alert_social_media_settings_not_updated'] = "خطأ في تحديث إعدادات الوسائط الاجتماعية!";
$lang['alert_seo_settings_updated'] = "تم تحديث إعدادات تحسين محركات البحث بنجاح!";
$lang['alert_seo_settings_not_updated'] = "خطأ في تحديث إعدادات تحسين محركات البحث!";
$lang['alert_permission_updated'] = "إذن محدث بنجاح!";
$lang['alert_permission_not_updated'] = "خطأ في تحديث الإذن!";
$lang['alert_app_settings_updated'] = "تم تحديث إعدادات التطبيق بنجاح!";
$lang['alert_app_settings_not_updated'] = "خطأ في تحديث إعدادات التطبيق!";
$lang['alert_enter_site_phone'] = "يرجى تقديم هاتف الموقع";
$lang['alert_enter_site_title'] = "يرجى تقديم عنوان الموقع";
$lang['alert_enter_meta_title'] = "يرجى تقديم عنوان التعريف";
$lang['alert_enter_meta_description'] = "يرجى تقديم وصف ميتا";
$lang['alert_enter_meta_keywords'] = "يرجى تقديم كلمات تعريف ميتا";
$lang['alert_login_success'] = "تسجيل الدخول بنجاح! ، ستتم إعادة التوجيه إلى لوحة التحكم";
$lang['alert_login_inactive'] = "المستخدم غير مفعل!";
$lang['alert_login_invalid'] = "بيانات الاعتماد غير صالحة";
$lang['alert_user_blocked'] = "المستخدم محظور !، يرجى الاتصال بالمسؤول.";
$lang['alert_user_notfound'] = "المستخدم ليس موجود!";
$lang['alert_forgot_password_success'] = "تم إرسال رابط إعادة التعيين بنجاح ، تحقق من بريدك الوارد";
$lang['alert_forgot_password_error'] = "خطأ في إرسال رابط إعادة تعيين!";
$lang['alert_reset_password_success'] = "إعادة تعيين كلمة المرور بنجاح";
$lang['alert_reset_password_error'] = "خطأ في إعادة تعيين كلمة المرور";
$lang['alert_reset_password_token_error'] = "رمز إعادة تعيين غير صالح";
$lang['alert_enter_ticket_title'] = "عنوان التذكرة مطلوب";
$lang['alert_enter_ticket_description'] = "وصف التذكرة مطلوب";
$lang['alert_select_ticket_category'] = "يرجى تحديد فئة التذكرة";
$lang['alert_select_ticket_priority'] = "يرجى تحديد أولوية التذكرة";
$lang['alert_ticket_created'] = "تذكرة تم إنشاؤها بنجاح!";
$lang['alert_ticket_not_created'] = "خطأ في إنشاء تذكرة!";
$lang['alert_are_you_sure'] = "هل أنت واثق!";
$lang['alert_delete_confirm'] = "بمجرد الحذف ، لا يمكن استعادة البيانات!";
$lang['alert_cancel'] = "ألغها";
$lang['alert_yes_delete'] = "نعم ، احذف";
$lang['alert_enter_subject'] = "يرجى إدخال الموضوع";
$lang['alert_enter_message'] = "من فضلك ادخل الرسالة";
$lang['alert_message_sent'] = "تم إرسال الرسالة بنجاح!";
$lang['alert_message_not_sent'] = "خطأ في إرسال الرسالة!";
$lang['alert_must_login'] = "يجب عليك تسجيل الدخول للقيام بهذا الإجراء!";
$lang['alert_error_on_voting'] = "خطأ في التصويت!";
$lang['alert_vote_success'] = "تم التصويت بنجاح على المقالة!";
$lang['alert_error_on_voting'] = "خطأ في مقال التصويت!";
$lang['alert_email_settings_updated'] = "تم تحديث إعدادات البريد الإلكتروني بنجاح!";
$lang['alert_email_settings_not_updated'] = "خطأ في تحديث إعدادات البريد الإلكتروني!";
$lang['alert_mail_from_title'] = "أدخل البريد من العنوان!";
$lang['alert_select_mail_driver'] = "الرجاء تحديد برنامج تشغيل البريد!";
$lang['alert_enter_from_email'] = "أدخل من البريد!";
$lang['alert_enter_template_name'] = "اسم القالب المطلوب!";
$lang['alert_enter_template_content'] = "محتوى القالب المطلوب!";
$lang['alert_update_template_success'] = "تم تحديث قالب البريد الإلكتروني بنجاح!";
$lang['alert_update_template_error'] = "خطأ في تحديث قالب البريد الإلكتروني!";